import $ from 'jquery';

/* Toggle More Content */
$(function(){

	var $container = $('[role="slide-toggle-container"]');
	var $trigger = $('[role="slide-toggle-trigger"]');
	var $target = $('[role="slide-toggle-target"]');

    $trigger.bind("click",function(e){

        var $this = $(this);
        var target = $this.closest($container).find($target);

        target.slideToggle();

        if($this.hasClass("active")){
            var movingtarget = $this.closest($container).attr("data-panel");
            $("html, body").animate({ scrollTop: $(movingtarget).offset().top - 100 });
        }
        
        $this.toggleClass("active");

    })


})