import $ from 'jquery'
import googleMapsLoader from 'load-google-maps-api'
//

export const apiKey = 'AIzaSyAmjOz9vdCVJTyNqZMKTjTVelFH30oD8f0'

export const defaultMinHeight = 250
export const defaultHeight = '100%'

const ensureElementHasHeight = (
  $el,
  minHeight = defaultMinHeight,
  height = defaultHeight
) => {
  if ($el.height() <= 0) {
    $el.css({ minHeight, height })
  }
}

const readOptionsFromData = $el => {
  const lat = +$el.data('map-lat')
  const lng = +$el.data('map-lng')
  const zoom = +$el.data('map-zoom')

  if (!lat) {
    throw new Error('Please add a "data-map-lat" attribute with the latitude.')
  } else if (!lng) {
    throw new Error('Please add a "data-map-lng" attribute with the longitude.')
  } else if (!zoom) {
    throw new Error(
      'Please add a "data-map-zoom" attribute with the map zoom level.'
    )
  }

  return {
    center: {
      lat,
      lng,
    },
    zoom,
  }
}

export const googleMaps = stylePredicate => {
  $(document).ready(() => {
    const loader = googleMapsLoader({ key: apiKey })

    $('[data-map]').each((index, el) => {
      const $el = $(el)

      let styles

      if (typeof stylePredicate === 'function') {
        styles = stylePredicate($el.data('map-style'))
      }

      ensureElementHasHeight($el)

      const options = { ...readOptionsFromData($el), ...{ styles } }

      $el.each((index, el) => {
        const $el = $(el)

        loader.then(google => {
          const map = new google.Map(el, options)

          const markerOptions = {
            position: options.center,
            map,
            icon: $el.data('map-marker-url') || null,
            animation: google.Animation.DROP,
          }

          const marker = new google.Marker({ position: options.center, map, icon: "./assets/img/marker_sm.png" })

          const infoWindow = new google.InfoWindow({
            content: `
<div class="map--info">
  <h3>
    Skywood Recovery
  </h3>
  <p>
    10499 North 48th St</br>Augusta, MI 49012
  </p>
  <a href="https://www.google.com/maps/place/10499+48th+St,+Augusta,+MI+49012/@42.4152253,-85.3025813,17z/data=!3m1!4b1!4m5!3m4!1s0x88178b1d838e9235:0x6752937d2f627651!8m2!3d42.4152214!4d-85.3003926" target="_blank" class="button primary">Get Directions</a>
</div>
  `,
          })

          marker.addListener('click', () => {
            infoWindow.open(map, marker)
          })
        })
      })
    })
  })
}

export default googleMaps
